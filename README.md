## Getting started

If you have just checked out his repository, you will need to run some commands to get up and running.
First, navigate to the root folder of this project on your local drive:

```bash
cd /local/path/to
```
Install Node modules:

```bash
npm install
```

Run a local web server to view the design template:

```bash
npm run serve
```

