var burgerButton = document.querySelector('.navigation__burger');
var navigationList = document.querySelector('.navigation-list');
var navigationLink = document.querySelector('.navigation__link');

burgerButton.addEventListener('click', function () {
    this.classList.toggle('clicked');
    navigationList.classList.toggle('active');
});

navigationLink.addEventListener('click', function () {
    navigationList.classList.remove('active');
})

$('.navigation__link').click(function () {
    navigationList.classList.remove('active');
    burgerButton.classList.toggle('clicked');
});